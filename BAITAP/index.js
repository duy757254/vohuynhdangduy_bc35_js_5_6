function result() {
  var standardPoint = document.getElementById("point").value * 1;
  var area = document.getElementById("txt-area").value * 1;
  var subject = document.getElementById("txt-subject").value * 1;
  var pointOne = document.getElementById("txt-point1").value * 1;
  var pointTwo = document.getElementById("txt-point2").value * 1;
  var pointThree = document.getElementById("txt-point3").value * 1;
  var result1 = "";
  if (pointOne <= 0) {
    result1 = "Bạn đã rớt.Do có điểm nhỏ hơn hoặc bằng 0";
    document.getElementById("total").innerHTML = `${result1}`;

    return;
  }
  if (pointTwo <= 0) {
    result1 = "Bạn đã rớt.Do có điểm nhỏ hơn hoặc bằng 0";
    document.getElementById("total").innerHTML = `${result1}`;
    return;
  }
  if (pointThree <= 0) {
    result1 = "Bạn đã rớt.Do có điểm nhỏ hơn hoặc bằng 0";
    document.getElementById("total").innerHTML = `${result1}`;
    return;
  }
  if (check(pointOne) && check(pointTwo) && check(pointThree)) {
    var total = pointOne + pointTwo + pointThree + area + subject;
    if (total >= standardPoint) {
      document.getElementById(
        "total"
      ).innerHTML = `Bạn đã đậu. tổng điểm là ${total}`;
    } else {
      document.getElementById(
        "total"
      ).innerHTML = `Bạn đã Rớt. tổng điểm là ${total}`;
    }
  }
  function check(point) {
    return point > 0;
  }
}

// BÀI 2: TÍNH TIỀN ĐIỆN

function bill() {
  var customer = document.getElementById("txt-customer").value;
  var kw = document.getElementById("txt-kw").value * 1;
  var bill = 0;
  if (kw > 0 && kw <= 50) {
    bill = kw * 500;
  } else if (kw > 50 && kw <= 100) {
    bill = 50 * 500 + (kw - 50) * 650;
  } else if (kw > 100 && kw <= 200) {
    bill = 50 * 500 + 50 * 650 + (kw - 100) * 850;
  } else if (kw > 200 && kw <= 350) {
    bill = 50 * 500 + 50 * 650 + 100 * 850 + (kw - 200) * 1110;
  } else if (kw > 350) {
    bill = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kw - 350) * 1300;
  } else {
    bill = "Không được nhập số Tiêu thụ (kw) nhỏ hơn 0";
  }
  
  bill = bill.toLocaleString('vi', {style : 'currency', currency : 'VND'});
console.log(bill);



  document.getElementById(
    "result2"
  ).innerHTML = `Khách Hàng: ${customer} <br> Số tiền phải trả là ${bill}`;
}
